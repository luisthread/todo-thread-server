import Task from './task.model';

export const taskByUserId = async (req, res) => {
	try {
		const { userId } = req.body;
		const tasks = await Task.find({ userId });

		res.status(200).json({ status: 200, payload: tasks });
	} catch (error) {
		res.status(403).json({ status: 403, message: error });
	}
};
