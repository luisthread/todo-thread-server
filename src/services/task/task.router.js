import { Router } from 'express';
import { taskByUserId } from './task.controller';

const router = Router();

router.get('/', taskByUserId);

export default router;
