import { Schema, model } from 'mongoose';

const taskSchema = new Schema({
	id: {
		type: String,
		required: true,
		unique: true
	},
	userId: {
		type: String,
		required: true
	},
	text: {
		type: String,
		required: true
	},
	completed: {
		type: Boolean,
		required: true
	}
});

export default model('Task', taskSchema);
