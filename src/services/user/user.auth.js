import jwt from 'jsonwebtoken';

const secret = 'Ozlt7nBV7PoAHlRY';

export const createToken = (payload) => {
	const token = jwt.sign(payload, secret, {
		expiresIn: 60 * 60 * 24
	});
	return token;
};

export const verifyToken = (req, res, next) => {
	try {
		const token = req.headers.authorization.split(' ')[1];
		const payload = jwt.verify(token, secret);
		next();
	} catch (error) {
		res.status(401).json({ auth: false, message: 'No authorized' });
	}
};
