import { Schema, model } from 'mongoose';
import bcrypt from 'bcrypt';

const userSchema = new Schema({
	id: {
		type: String,
		required: true,
		unique: true
	},
	name: {
		type: String,
		required: true
	},
	username: {
		type: String,
		required: true,
		unique: true
	},
	email: {
		type: String,
		required: true,
		unique: true
	},
	password: {
		type: String,
		required: true
	}
});

userSchema.methods.encryptPassword = async function(password) {
	const salt = await bcrypt.genSalt(12);
	return bcrypt.hash(password, salt);
};

userSchema.methods.validatePassword = async function(password) {
	return bcrypt.compare(password, this.password);
};

export default model('User', userSchema);
