import { nanoid } from 'nanoid';
import User from './user.model';
import { createToken } from './user.auth';

export const signup = async (req, res) => {
	try {
		const { name, username, email, password } = req.body;

		const user = new User({
			id: nanoid(12),
			name,
			username,
			email,
			password
		});

		user.password = await user.encryptPassword(user.password);

		const saved = await user.save();

		const token = createToken({ username, password });

		res.status(200).json({ auth: true, token, payload: saved });
	} catch (error) {
		res.status(403).json({ auth: false, message: error });
	}
};

export const signin = async (req, res) => {
	try {
		const { username, password } = req.body;

		const user = await User.findOne({ username });
		if (!user) {
			throw 'User not exist';
		}

		const isValidPassword = await user.validatePassword(password);
		if (!isValidPassword) {
			throw 'Incorrect password';
		}

		const token = createToken({ username, password });

		res.status(200).json({ auth: true, token, payload: user });
	} catch (error) {
		res.status(403).json({ auth: false, message: error });
	}
};
