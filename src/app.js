import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import dotnev from 'dotenv';
import userRouter from './services/user/user.router';
import taskRouter from './services/task/task.router';
import { verifyToken } from './services/user/user.auth';

const app = express();

dotnev.config();
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(cors());
app.use(morgan('dev'));

app.set('port', process.env.PORT || 4000);

app.use('/user', userRouter);
app.use('/task', verifyToken, taskRouter);

export default app;
